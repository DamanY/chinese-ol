# Chinese Online (CHINESEOL)

#### Description

Chinese Article Online Service ( Chinese OL )
Chinese Article Website for schools, teachers, and students.
Made By Daman.
https://damanyang.cn

#### Software Architecture
- NodeJS
- NPM
- VUE 2

#### Installation

1. ```
   npm i
   ```

2. ```
   npm run serve
   ```

3. ```
   npm run build
   ```

#### Instructions

Here is the UI part of the whole service, the config can be seen in the SERVER part.

#### Document

[document](https://damany.gitee.io/chinese-ol/)

#### Contribution

1.  Fork the repository
2.  Contact me: Tencent QQ 2402664612

#### About Developers & Website

- [Daman](https://damanyang.cn)

- [Official](https://jxjlife.cn)

#### 其他语言/Other languages

[中文/Chinese](/README.md)

[英语/English](/README.en.md)

---

All rights reserved 2020 - present

**MADE IN CHINA**