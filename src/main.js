import Vue from 'vue'
import './plugins/axios'
import App from './App.vue'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify';

import api from './request/api';
import axios from "axios";

import MDATC from './components/md-atc'
import CCMT from './components/c-cmt'
import CREPL from './components/c-repl'

Vue.use(api);

Vue.component('md-atc',MDATC);
Vue.component('c-cmt',CCMT);
Vue.component('c-repl',CREPL);

Vue.config.productionTip = false

axios.defaults.withCredentials = true
// The url of the service
axios.defaults.baseURL = 'http://daman.tpddns.cn:99'

router.beforeEach((to, from, next) => {
  scrollTo(0,0)
  next()
})

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')
