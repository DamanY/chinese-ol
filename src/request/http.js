import axios from 'axios';

export function get(url) {
    return axios.get(url);
}

export function post(url, param) {
    return axios.post(url, param);
}
// export function get(url) {
//     return new Promise((resolve, reject) => {
//         axios.get(url)
//             .then(res => {
//                 resolve(res.data);
//             })
//             .catch(err => {
//                 reject(err.data)
//             })
//     });
// }
//
// export function post(url, param) {
//     return new Promise((resolve, reject) => {
//         axios.post(url, param)
//             .then(res => {
//                 resolve(res.data);
//             })
//             .catch(err => {
//                 reject(err.data)
//             })
//     });
// }
