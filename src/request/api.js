import { get, post } from './http';

export default{
    install(Vue,options)
    {
        Vue.prototype.getUser = function () {
            return get('/user/getUser');
        }
        Vue.prototype.getIndex = function (page, size) {
            return get('/atc/getIndex?page='+page+'&size='+size);
        }
        Vue.prototype.getCmt = function (aid) {
            return get('/cmt/get/'+aid);
        }
        Vue.prototype.getAtcBy = function (id) {
            return get('/atc/getById/'+id);
        }
        Vue.prototype.getReply = function (aid) {
            return get('/repl/get/'+aid);
        }
        Vue.prototype.userLogin = function (user) {
            return post('/user/login', user);
        }
        Vue.prototype.userSign = function (user) {
            return post('/user/sign', user);
        }
        Vue.prototype.logout = function () {
            get('/user/logout');
        }
        Vue.prototype.getUserBy = function (id) {
            return get('/user/getUserBy/'+id);
        }
        Vue.prototype.sendCmt = function (cmt) {
            return post('/cmt/send', cmt);
        }
        Vue.prototype.sendRepl = function (repl) {
            return post('/repl/send', repl);
        }

        Vue.prototype.getTopAtc = function () {
            return get('/atc/getTop')
        }
        Vue.prototype.getByLights = function () {
            return get('/atc/getByLights?size=5')
        }
        Vue.prototype.getByLikes = function () {
            return get('/atc/getByLikes?size=5')
        }

        Vue.prototype.likeAtc = function (id) {
            return get('/do/like?id='+id);
        }
        Vue.prototype.lightAtc = function (id) {
            return get('/do/light?id='+id);
        }
        Vue.prototype.favorAtc = function (id) {
            return get('/do/favor?id='+id);
        }

        Vue.prototype.searchAtc = function (wd) {
            return get('/atc/search?wd='+wd);
        }
        Vue.prototype.getMsg = function (page) {
            return get('/msg/get/'+page);
        }
        Vue.prototype.getMsg2 = function (page, size) {
            return get('/msg/get/'+page+'?size='+size);
        }

        Vue.prototype.sendMsg = function (msg) {
            return post('/msg/send', msg);
        }

        Vue.prototype.getConf = function (key) {
            return get('/conf/get/'+key);
        }
        Vue.prototype.getInfoByUid = function (uid) {
            return get('/atc/getInfoByUid/'+uid);
        }
    }
}

