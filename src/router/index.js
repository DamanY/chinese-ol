import Vue from 'vue'
import VueRouter from 'vue-router'

const Index = () => import('../views/Index')

const Home = () => import('../views/base/Home')
const Msg = () => import('../views/base/Msg')
const Search = () => import('../views/base/Search')
const About = () => import('../views/base/About')
const Article = () => import('../views/base/Article')
const User = () => import('../views/base/User')

const Login = () => import('../views/user/login')
const Sign = () => import('../views/user/sign')
const NotFound = () => import('../views/other/404')

Vue.use(VueRouter)

// 解决vue-router在3.0版本以上重复点菜单报错问题
const originalPush = VueRouter.prototype.push
VueRouter.prototype.push = function push(location) {
  return originalPush.call(this, location).catch(err => err)
}

const routes = [
  {
    path: '/',
    name: 'Index',
    component: Index,
    children: [
      {
        path: '/',
        name: 'Home',
        component: Home,
        meta: {
          text: '首页',
          icon: 'mdi-home',
        }
      },
      {
        path: '/search',
        name: 'Search',
        component: Search,
        meta: {
          text: '搜索',
          icon: 'mdi-database-search',
        }
      },
      {
        path: '/msg',
        name: 'Message',
        component: Msg,
        meta: {
          text: '留言板',
          icon: 'mdi-forum',
        }
      },
      {
        path: '/about',
        name: 'About',
        component: About,
        meta: {
          text: '关于',
          icon: 'mdi-information',
        }
      },
      {
        path: '/article/*',
        name: 'Article',
        component: Article,
        meta: {
          text: '文章'
        }
      },
      {
        path: '/user/*',
        name: 'User',
        component: User,
        meta: {
          text: '用户'
        }
      }
    ]
  },
  {
    path: '/login',
    name: 'Login',
    component: Login,
  },
  {
    path: '/sign',
    name: 'Sign',
    component: Sign,
  },
  {
    path: '/*',
    name: '404',
    component: NotFound,
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
