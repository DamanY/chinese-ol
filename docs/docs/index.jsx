---
banner:
  name: '语文文章网站-官方文档'
  desc: 'Chinese Article Web Service - Official Document'
  btns: 
    - { name: '开 始', href: './documents/index.html', primary: true }
    - { name: 'Gitee >', href: 'https://gitee.com/DamanY/chinese-ol' }
  caption: '状态：开发/维护中'
features: 
  - { name: '优雅', desc: '经过精雕细琢，我们带给大家一个精心设计的、拥有卓越的视觉与交互体验的网站' }
  - { name: '灵动', desc: '我们拥有非常灵活的功能，正在努力构建活跃的社区。也许初次使用未见其惊艳，但当你使用后便会发现她的强大' }
  - { name: '简洁', desc: '以 ‘无形’ 代替 ‘有形’，从开发体验到用户界面的呈现，不断去除冗余的设计，使用户专注于写作与阅读' }
  - { name: '开源', desc: '作为开源项目，我们拥有高质量的代码、完善的自动化测试流程，对社区的需求能够作出积极快速响应' }

footer:
  copyRight:
    name: 'Daman'
    href: '#'
  links:
    开发者网址:
      - { name: 'Daman', href: 'https://damanyang.cn' }
    项目:
      - { name: 'Gitee', href: 'https://gitee.com/DamanY/chinese-ol' }
      - { name: 'Website', href: 'https://gitee.com/DamanY/chinese-ol' }
    开源维护:
      - { name: 'Apache License 2.0', href: 'https://gitee.com/DamanY/chinese-ol/blob/master/LICENSE' }
      - { name: 'Daman: 2402664612 (QQ)', href: '#' }
---

<Homepage banner={banner} features={features} />
<Footer distPath={props.page.distPath} copyRight={props.footer.copyRight} links={props.footer.links} />