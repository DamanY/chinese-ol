window.ydoc_plugin_search_json = {
  "文档": [
    {
      "title": "ChineseOL docs",
      "content": "本文档将介绍ChineseOL项目以及如何安装、使用。",
      "url": "\\documents\\index.html",
      "children": []
    },
    {
      "title": "简介",
      "content": "",
      "url": "\\documents\\intro.html",
      "children": [
        {
          "title": "what",
          "url": "\\documents\\intro.html#what",
          "content": "what语文文章网站（网络服务），供学校、师生使用。Chinese Article Online Service ( Chinese OL )Chinese Article Website for schools, teachers, and students.Made By Daman.https://damanyang.cn"
        },
        {
          "title": "软件架构",
          "url": "\\documents\\intro.html#软件架构",
          "content": "软件架构NodeJS\nNPM\nVUE 2\n"
        }
      ]
    },
    {
      "title": "环境准备",
      "content": "\n前端\n\n\nNodeJS\n\n\nNPM\n\n\nvue-cli\nnpm i -g @vue/cli\n\n\n\n\n\n后端\n\nJava 8\nMaven\n\n\n",
      "url": "\\documents\\env.html",
      "children": []
    },
    {
      "title": "安装",
      "content": "准备环境\n前端\n\n\n\n\n后端\n\n\n\n",
      "url": "\\documents\\install.html",
      "children": []
    },
    {
      "title": "",
      "content": "",
      "url": "\\documents\\desi.html",
      "children": []
    },
    {
      "title": "",
      "content": "",
      "url": "\\documents\\serv.html",
      "children": []
    }
  ],
  "关于": [
    {
      "title": "About",
      "content": "GiteeOfficial WebsiteDaman's Blog",
      "url": "\\about.html",
      "children": []
    }
  ]
}