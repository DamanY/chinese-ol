# ChineseOL

#### 介绍
语文文章网站（网络服务），供学校、师生使用。
Chinese Article Online Service ( Chinese OL )
Chinese Article Website for schools, teachers, and students.
Made By Daman.
https://damanyang.cn

#### 软件架构
- NodeJS
- NPM
- VUE 2


#### 安装教程

1. ```
   npm i
   ```

2. ```
   npm run serve
   ```

3. ```
   npm run build
   ```

#### 使用说明

本部分是本项目的前端部分，配置见后端。

#### 文档

[document](https://damany.gitee.io/chinese-ol/)

#### 参与贡献

1.  Fork 本仓库
2.  联系开发者：QQ 2402664612

#### About Developers & Website

- [Daman](https://damanyang.cn)

- [Official](https://jxjlife.cn)

#### 其他语言/Other languages

[中文/Chinese](/README.md)

[英语/English](/README.en.md)

---

All rights reserved 2020 - present

**MADE IN CHINA**